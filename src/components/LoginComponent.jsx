import './LoginComponent.css';
import { UserContext } from '../context/UserContextComponent';
import { UserContextProvider } from '../context/UserContextComponent';
import { useState } from 'react';
import { UserContexts } from '../context/UserContextComponent';

const LoginComponent=()=>{
    const [values,setValues]=useState({
        name:'',
        password:''
    })
    const {logIn,userName}=UserContexts();
    console.log(userName)
    const handleChange=(e)=>{
        e.preventDefault();
        setValues({...values,[e.target.name]:e.target.value});
    }
    const handleSubmit=(e)=>{
        e.preventDefault();
        logIn(values.name);
        console.log(values);
    }
     return (
         <UserContextProvider>
            <form onSubmit={handleSubmit}>
            <div className="login-form">
        <label>Name</label>
        <input type="text" name='name' value={values.name} onChange={handleChange}/>
        <label>Password</label>
        <input type="text" name='password' value={values.password} onChange={handleChange}/>
        <button>Submit</button>
        </div>
            </form>
           
            
     
        </UserContextProvider>
    )
}

export default LoginComponent;