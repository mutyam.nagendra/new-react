const FormNewUseStateComponent=(props)=>{
    return <div className="form-new">
        <label>{props.label}</label>
      <input type="text" onChange={props.onChange} name={props.name} placeholder={props.placeholder}/>
    </div>
}

export default FormNewUseStateComponent;