import { useEffect } from "react";
import { useState } from "react"

const HomeComponent =()=>{
    const [count,setCount]=useState(0);
    // const [username,setusername]=useState('');
    // const [booleanValue,setbooleanValue]=useState(false);
    const [state,setState]=useState({
        username:'',
        booleanValue:false
    })
    const handleChange=(e)=>{
     setState({...state,[e.target.name]:e.target.value})
    }
    const handleClick=(e)=>{
        setState({...state,booleanValue:true})
       }

    console.count('Component rerender');
    useEffect(()=>{
      console.log('useEffect runs!');
      document.title=`you clicked ${count} times`
    },[state])
    return <div>
        You clicked {count} times
        <input type="text" name='username'  onChange={handleChange}/>
        <button onClick={()=>setCount(count+1)}>Count</button>
        <button onCLick={handleClick}>Select</button>
    </div>
}

export default HomeComponent