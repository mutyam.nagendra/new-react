import { useState } from "react";

const PropDrillingContextAPIComponenet=()=>{
 const [value,setValue]=useState('Value');
 const [newvalue,setnewValue]=useState('New Value');
    return <div>
        <h1>First Component</h1>
       <Child1 value={value} newValue={newvalue}/>
    </div>
}
const Child1=(props)=>{
       return <div>
         <h1>Second Component</h1>
          <Child2 value={props.value} newValue={props.newValue}/>
       </div>
   }
   const Child2=(props)=>{
       return <div>
         <h1>Third Component</h1>
          <Child3 value={props.value} newValue={props.newValue}/>
       </div>
   }
   const Child3=(props)=>{
       return <div>
          <h1>Fourth Component</h1>
          {props.value} <br/>
          {props.newValue}
       </div>
   }

export default PropDrillingContextAPIComponenet;