import React,{ useState } from "react";
import FormInputComponent  from "./FormInputComponent";
import FormNewInputComponent from "./FormNewInputComponent";
import FormNewUseStateComponent from "./FormNewUseStateComponent";
import './AboutComponent.css';
import { useRef } from "react";
const AboutComponent =()=>{
    const [values,setValues]=useState({
        firstname:'',
        lastname:'',
        email:'',
        designation:'',
        project:''
    });
    
    const inputs =[
        {
            id:1,
            name:'firstname',
            placeholder:'First Name',
            type:'text',
            label:'First Name'
        },
        {
            id:2,
            name:'lastname',
            placeholder:'Last Name',
            type:'text',
            label:'Last Name'
        },
        {
            id:3,
            name:'email',
            placeholder:'Email',
            type:'text',
            label:'Email'
        },
        {
            id:4,
            name:'designation',
            placeholder:'Designation',
            type:'text',
            label:'Designation'
        },
        {
            id:5,
            name:'project',
            placeholder:'Project',
            type:'text',
            label:'Project'
        },

    ]
    const fname=useRef();
    const lname=useRef();
    const email=useRef();
    const designation=useRef();
    const project=useRef();
    const handleSubmit=(e)=>{
      e.preventDefault();
      const data = new FormData(e.target);
      console.log(Object.fromEntries(data.entries()));
    //  console.log(userRefer.current.value)
    }
    const handleSubmit1=(e)=>{
        e.preventDefault();
        const data={
            firstname:fname.current.value,
            lastname:lname.current.value,
            email:email.current.value,
            designation:designation.current.value,
            project:project.current.value,
        }
        console.log(data);
      }

      const handleSubmit2 =(e)=>{
        e.preventDefault();
        console.log(values);
      }
      const handleChangeEvent=(e)=>{
        setValues({...values,[e.target.name]:e.target.value});
       
      }
      console.log(values);
    return (<div className="new-form">
        <div className="child-container-1">
        <form onSubmit={handleSubmit}>
        <FormInputComponent name='firstname' placeholder="First Name" />
        <FormInputComponent name='lastname' placeholder="Last Name"/>
        <FormInputComponent name='email' placeholder="Email"/>
        <FormInputComponent name='designation'placeholder="Designation"/>
        <FormInputComponent name='project' placeholder="Project"/>
        <button>Submit</button>
        </form>
        </div>
        <div className="child-container-2">
        <form onSubmit={handleSubmit1}>
        <FormNewInputComponent name='firstname' placeholder="First Name" refer={fname} />
        <FormNewInputComponent name='lastname' placeholder="Last Name" refer={lname}/>
        <FormNewInputComponent name='email' placeholder="Email" refer={email}/>
        <FormNewInputComponent name='designation'placeholder="Designation" refer={designation}/>
        <FormNewInputComponent name='project' placeholder="Project" refer={project}/>
        <button>Submit</button>
        </form>
        </div>
        <div className="child-container-3">
        <form onSubmit={handleSubmit2}>
           {
            inputs.map((input)=>(
                <FormNewUseStateComponent onChange={handleChangeEvent} key={input.id} {...input} value={values[input.name] } />
            ))
           }
            <button>Submit</button>
        </form>
       
        </div>
        
        
    </div>);
}

export default AboutComponent;