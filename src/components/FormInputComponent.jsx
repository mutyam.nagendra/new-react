import "./FormInputComponent.css"
const FormInputComponent = (props) => {
    return <div className="formInput">
        <label>{props.placeholder}</label>
        <input type="text" name={props.name}  placeholder={props.placeholder}/>
    </div>
}

export default FormInputComponent;