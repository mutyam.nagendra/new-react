const FormNewInputComponent =(props)=>{
  return <div>
    <label>{props.placeholder}</label>
    <input type='text' name={props.name} ref={props.refer} placeholder={props.placeholder}/>
  </div>
}
export default FormNewInputComponent;