import ComponentA from "./ComponentA";
import ComponentB from "./ComponentB";
import { useState } from "react";
const StateLiftComponent =()=>{
    const [value,setValue]=useState('');
    const handleChange=(e)=>{
      e.preventDefault();
      setValue(e.target.value);
    }
     return <div>
        <ComponentA svalue={value} handleChange={handleChange}/>
        <ComponentB svalue={value}/>
     </div>
}
export default StateLiftComponent;