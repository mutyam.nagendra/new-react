import { useContext } from "react";
import { useState } from "react";
import { createContext } from "react";

export const userContext=createContext({
    userName:null,
    logIn:()=>{},
    logOut:()=>{}
})
const USER={name:'Guest',isGuestUser:true}
export const UserContextProvider=({children})=>{
    const [username,setUserName]=useState(USER);
    function logIn(username){
        setUserName({isGuestUser:false,name:username})
    }
    function logOut(username){
        setUserName(USER )
    }
    return (
        <userContext.Provider  value={{username,logIn,logOut}}>
            {children}
        </userContext.Provider>
    )
}

export const UserContexts=()=>{
     const {userName,login,logout}=useContext(userContext);
     return {userName,login,logout};
}