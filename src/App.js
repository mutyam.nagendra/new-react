import logo from './logo.svg';
import './App.css';
import { Route, Routes } from 'react-router-dom';
import AboutComponent from './components/AboutComponent';
import NavbarComponent from './components/NavbarComponenet';
import HomeComponent from './components/HomeComponent';
import ContactUsComponent from './components/ContactUsComponent';
import StateLiftComponent from './components/StateLiftComponent';
import PropDrillingContextAPIComponenet from './components/PropDrillingContextAPIComponent';
import LoginComponent from './components/LoginComponent';

function App() {
  const Abouts = () => <h1>About Us</h1>

  return (
    <div className="App">
      <NavbarComponent/>
      <Routes >
      <Route path="/" exact element={<HomeComponent/>}/>
        <Route path="/about/" element={<AboutComponent/>}/>
        <Route path="/contactus" element={<ContactUsComponent/>}/>
        <Route path="/state" element={<StateLiftComponent/>}/>
        <Route path="/propdrill" element={<PropDrillingContextAPIComponenet/>}/>
        <Route path="/login" element={<LoginComponent/>}/>
         </Routes >
      
    </div>
  );
}

export default App;
